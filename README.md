# base-admin

#### 介绍
base-admin轻量级脚手架,基于ElementUi、Springboot、SpringSecurity、 MybatisPlus、Redis、Mysql、Maven实现.

#### 快速开始

1. 创建数据库,导入resource/sql/base-tiny.sql
2. 导入项目, 准备好redis、mysql相关应用
3. 修改redis、mysql等相关配置
4. 启动服务端项目
5. 启动Admin前端base-tiny-ui项目
5. 启动APP端base-tiny-app项目
7. 访问Admin端 http://localhost:8098, 账号/密码: admin/admin123
8. 访问App端 http://localhost:8089, 账号/密码: 18877514905/123456

### 在线体验

H5演示: http://42.193.244.152/knowledge-app 账号/密码: 18877514905/123456

演示地址：http://42.193.244.152/knowledge-admin 账号/密码:admin/admin123

#### 运行截图

<table>
    <tr>
        <td><img src="screenshot/1.png"/></td>
        <td><img src="screenshot/2.png"/></td>
        <td><img src="screenshot/3.png"/></td>   
    </tr>
    <tr>
        <td colspan="3"><img src="screenshot/4.png"/></td>   
    </tr>
    <tr>
        <td colspan="3"><img src="screenshot/5.png"/></td>
    </tr>
    <tr>
        <td colspan="3"><img src="screenshot/6.png"/></td>
    </tr>	 
</table>

