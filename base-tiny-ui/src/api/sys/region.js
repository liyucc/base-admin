import request from '@/utils/request';

export function getSelectRegionList(parentId, params) {
  return request({
    url: '/region/list/' + parentId,
    method: 'get',
    params: params
  })
}

export function fetchRegionList(parentId, params) {
  return request({
    url: '/region/listPage/' + parentId,
    method: 'get',
    params: params
  })
}

export function getRegionInfo(id) {
  return request({
    url: '/region/info/' + id,
    method: 'get',
  })
}

export function createRegion(data) {
  return request({
    url: '/region/save',
    method: 'post',
    data: data
  })
}

export function updateRegion(data) {
  return request({
    url: '/region/update',
    method: 'post',
    data: data
  })
}

export function deleteRegion(data) {
  return request({
    url: '/region/delete',
    method: 'post',
    data: data
  })
}