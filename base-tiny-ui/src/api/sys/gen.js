import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/gen/list',
    method: 'get',
    params: query
  })
}