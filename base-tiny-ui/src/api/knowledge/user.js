import request from '@/utils/request';

export function fetchUserList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}

export function getUserInfo(id) {
  return request({
    url: '/user/info/' + id,
    method: 'get',
  })
}

export function createUser(data) {
  return request({
    url: '/user/save',
    method: 'post',
    data: data
  })
}

export function updateUser(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data: data
  })
}

export function deleteUser(data) {
  return request({
    url: '/user/delete',
    method: 'post',
    data: data
  })
}