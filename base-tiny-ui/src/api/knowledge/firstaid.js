import request from '@/utils/request';

export function fetchFirstAidList(query) {
  return request({
    url: '/firstaid/list',
    method: 'get',
    params: query
  })
}

export function getFirstAidInfo(id) {
  return request({
    url: '/firstaid/info/' + id,
    method: 'get',
  })
}

export function createFirstAid(data) {
  return request({
    url: '/firstaid/save',
    method: 'post',
    data: data
  })
}

export function updateFirstAid(data) {
  return request({
    url: '/firstaid/update',
    method: 'post',
    data: data
  })
}

export function deleteFirstAid(data) {
  return request({
    url: '/firstaid/delete',
    method: 'post',
    data: data
  })
}