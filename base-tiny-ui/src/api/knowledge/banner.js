import request from '@/utils/request';

export function fetchBannerList(query) {
  return request({
    url: '/banner/list',
    method: 'get',
    params: query
  })
}

export function getBannerInfo(id) {
  return request({
    url: '/banner/info/' + id,
    method: 'get',
  })
}

export function createBanner(data) {
  return request({
    url: '/banner/save',
    method: 'post',
    data: data
  })
}

export function updateBanner(data) {
  return request({
    url: '/banner/update',
    method: 'post',
    data: data
  })
}

export function deleteBanner(data) {
  return request({
    url: '/banner/delete',
    method: 'post',
    data: data
  })
}