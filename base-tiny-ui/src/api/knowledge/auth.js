import request from '@/utils/request';

export function fetchAuthList(query) {
  return request({
    url: '/auth/list',
    method: 'get',
    params: query
  })
}

export function getAuthInfo(id) {
  return request({
    url: '/auth/info/' + id,
    method: 'get',
  })
}

export function createAuth(data) {
  return request({
    url: '/auth/save',
    method: 'post',
    data: data
  })
}

export function updateAuth(data) {
  return request({
    url: '/auth/update',
    method: 'post',
    data: data
  })
}

export function deleteAuth(data) {
  return request({
    url: '/auth/delete',
    method: 'post',
    data: data
  })
}