import request from '@/utils/request';

export function fetchFirstaidTypeList(query) {
  return request({
    url: '/firstaidtype/list',
    method: 'get',
    params: query
  })
}

export function getFirstaidTypeInfo(id) {
  return request({
    url: '/firstaidtype/info/' + id,
    method: 'get',
  })
}

export function createFirstaidType(data) {
  return request({
    url: '/firstaidtype/save',
    method: 'post',
    data: data
  })
}

export function updateFirstaidType(data) {
  return request({
    url: '/firstaidtype/update',
    method: 'post',
    data: data
  })
}

export function deleteFirstaidType(data) {
  return request({
    url: '/firstaidtype/delete',
    method: 'post',
    data: data
  })
}