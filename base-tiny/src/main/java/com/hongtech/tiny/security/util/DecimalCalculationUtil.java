package com.hongtech.tiny.security.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 数值计算工具类型 scale 为小数点位数
 */
public class DecimalCalculationUtil {

    private DecimalCalculationUtil(){}

    public static BigDecimal divide(BigDecimal one,  BigDecimal two){
        return DecimalCalculationUtil.divide(one, two, 2);
    }

    public static BigDecimal divide(BigDecimal one,  BigDecimal two, int scale){
        return one.divide(two, scale, RoundingMode.HALF_UP);
    }

    public static BigDecimal multiply(BigDecimal one,  BigDecimal two){
        return DecimalCalculationUtil.multiply(one, two, 2);
    }

    public static BigDecimal multiply(BigDecimal one,  BigDecimal two, int scale){
        return one.multiply(two).setScale(scale, RoundingMode.HALF_UP);
    }

    public static BigDecimal add(BigDecimal one,  BigDecimal two){
        return DecimalCalculationUtil.add(one, two, 2);
    }

    public static BigDecimal add(BigDecimal one,  BigDecimal two, int scale){
        return one.add(two).setScale(scale, RoundingMode.HALF_UP);
    }

    public static BigDecimal subtract(BigDecimal one,  BigDecimal two){
        return DecimalCalculationUtil.subtract(one, two, 2);
    }

    public static BigDecimal subtract(BigDecimal one,  BigDecimal two, int scale){
        return one.subtract(two).setScale(scale, RoundingMode.HALF_UP);
    }

}
