package com.hongtech.tiny.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiV2UserGetRequest;
import com.dingtalk.api.response.OapiV2UserGetResponse;
import com.dingtalk.open.app.api.OpenDingTalkStreamClientBuilder;
import com.dingtalk.open.app.api.security.AuthClientCredential;
import com.dingtalk.open.app.stream.protocol.event.EventAckStatus;
import com.hongtech.tiny.common.service.DingTalkAccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DingTalkStreamConfig {

    @Value("${app.appKey:}")
    private String appKey;

    @Value("${app.appSecret:}")
    private String appSecret;

    /**
     * 配置OpenDingTalkClient客户端并配置初始化方法(start)
     */
    @Bean
    public void configureStreamClient() throws Exception {
        // init stream client
        OpenDingTalkStreamClientBuilder.custom()
                //配置应用的身份信息, 企业内部应用分别为appKey和appSecret, 三方应用为suiteKey和suiteSecret
                .credential(new AuthClientCredential(appKey, appSecret))
                //注册事件监听
                .registerAllEventListener(event -> {
                    try {
                        //事件唯一Id
                        String eventId = event.getEventId();
                        //事件类型
                        String eventType = event.getEventType();
                        //事件产生时间
                        Long bornTime = event.getEventBornTime();
                        //获取事件体
                        JSONObject bizData = event.getData();
                        //处理事件
                        System.out.println("event: " + bizData.toJSONString());
                        JSONArray jsonArray = bizData.getJSONArray("userId");
                        for (int i = 0; i < jsonArray.size(); i++) {
                            String userId = jsonArray.getString(i);
                            this.getUserInfoByUserId(userId);
                        }
                        // 消费成功
                        return EventAckStatus.SUCCESS;
                    } catch (Exception e) {
                        // 消费失败
                        return EventAckStatus.LATER;
                    }
                }).build().start();
    }

    public void getUserInfoByUserId(String userId) {
        try {
            DingTalkAccessToken dingTalkAccessToken = new DingTalkAccessToken(appKey, appSecret);
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/get");
            OapiV2UserGetRequest req = new OapiV2UserGetRequest();
            req.setUserid(userId);
            req.setLanguage("zh_CN");
            OapiV2UserGetResponse rsp = client.execute(req, dingTalkAccessToken.getAccessToken());
            OapiV2UserGetResponse.UserGetResponse result = rsp.getResult();
            System.out.println("用户数据：" + JSONObject.toJSONString(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
