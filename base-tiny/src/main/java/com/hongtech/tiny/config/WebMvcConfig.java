package com.hongtech.tiny.config;

import com.hongtech.tiny.modules.app.interceptor.AppAuthInterceptor;
import com.hongtech.tiny.modules.app.resolver.AppLoginUserResolver;
import com.hongtech.tiny.modules.oss.config.FileProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 文件路径信息配置
     */
    private final FileProperties properties;

    /**
     * app端登录拦截
     */
    @Resource
    private AppAuthInterceptor authInterceptor;

    /**
     * LoginUser注解的方法参数，注入当前登录用户
     */
    @Resource
    private AppLoginUserResolver appLoginUserResolver;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor).addPathPatterns("/app/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(appLoginUserResolver);
    }

    public WebMvcConfig(FileProperties properties) {
        this.properties = properties;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        FileProperties.ElPath path = properties.getPath();
        String pathUtl = "file:" + path.getPath().replace("\\","/");
        registry.addResourceHandler("/file/**").addResourceLocations(pathUtl).setCachePeriod(0);
    }

}
