package com.hongtech.tiny.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatis 全局插入创建时间和更新时间
 */
@Component
public class MybatisMetaHandler implements MetaObjectHandler {

    /**
     * 插入数据自动填充创建时间、更新时间
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Date nowTime = new Date();
        this.strictInsertFill(metaObject, "createTime", Date.class, nowTime);
        this.strictInsertFill(metaObject, "updateTime", Date.class, nowTime);
    }

    /**
     * 更新数据自动调
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Date nowTime = new Date();
        this.strictUpdateFill(metaObject, "updateTime", Date.class, nowTime);
    }

}

