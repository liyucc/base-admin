package com.hongtech.tiny;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseTinyApplication {

    private static final Logger log = LoggerFactory.getLogger(BaseTinyApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BaseTinyApplication.class, args);
        log.info("----------------------@@@项目启动成功@@@---------------------");
    }

}
