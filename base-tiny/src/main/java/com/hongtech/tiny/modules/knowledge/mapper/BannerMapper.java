package com.hongtech.tiny.modules.knowledge.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.modules.knowledge.entity.BannerEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 横幅表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Mapper
public interface BannerMapper extends BaseMapper<BannerEntity> {

    Page<BannerEntity> listBannerByPage(Page<BannerEntity> page, @Param("query" ) Map<String, Object> params);
	
}