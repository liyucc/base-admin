package com.hongtech.tiny.modules.oss.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.oss.entity.QiniuConfigEntity;
import com.hongtech.tiny.modules.oss.entity.QiniuFileEntity;
import com.hongtech.tiny.modules.oss.vo.QiniuFileQueryCriteria;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface QiniuFileService extends IService<QiniuFileEntity> {

    /**
     * 分页查询
     *
     * @param criteria 条件
     * @param page     分页参数
     * @return /
     */
    PageResult<QiniuFileEntity> queryAll(QiniuFileQueryCriteria criteria, Page<QiniuFileEntity> page);

    /**
     * 查询全部
     * @param criteria 条件
     * @return /
     */
    List<QiniuFileEntity> queryAll(QiniuFileQueryCriteria criteria);

    /**
     * 上传文件
     * @param file 文件
     * @param qiniuConfigEntity 配置
     * @return QiniuContent
     */
    QiniuFileEntity upload(MultipartFile file, QiniuConfigEntity qiniuConfigEntity);

    /**
     * 下载文件
     * @param content 文件信息
     * @param config 配置
     * @return String
     */
    String download(QiniuFileEntity content, QiniuConfigEntity config);

    /**
     * 删除文件
     * @param content 文件
     * @param config 配置
     */
    void delete(QiniuFileEntity content, QiniuConfigEntity config);

    /**
     * 同步数据
     * @param config 配置
     */
    void synchronize(QiniuConfigEntity config);

    /**
     * 删除文件
     * @param ids 文件ID数组
     * @param config 配置
     */
    void deleteAll(Long[] ids, QiniuConfigEntity config);

    /**
     * 导出数据
     * @param queryAll /
     * @param response /
     * @throws IOException /
     */
    void downloadList(List<QiniuFileEntity> queryAll, HttpServletResponse response) throws IOException;

}
