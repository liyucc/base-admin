package com.hongtech.tiny.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 后台资源表
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_resource")
@ApiModel(value="SysResource对象", description="后台资源表")
public class SysResource implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotBlank(message = "请填写资源名称")
    @ApiModelProperty(value = "资源名称")
    private String name;

    @NotBlank(message = "请填写资源URL")
    @ApiModelProperty(value = "资源URL")
    private String url;

    @NotBlank(message = "请填写资源描述")
    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "资源分类ID")
    private Long categoryId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
