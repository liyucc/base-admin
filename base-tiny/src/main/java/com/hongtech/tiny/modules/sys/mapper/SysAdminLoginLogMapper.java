package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysAdminLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户登录日志表 Mapper 接口
 * </p>
 */
public interface SysAdminLoginLogMapper extends BaseMapper<SysAdminLoginLog> {

}
