package com.hongtech.tiny.modules.knowledge.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.knowledge.entity.BannerEntity;
import com.hongtech.tiny.modules.knowledge.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.Map;


/**
 * 横幅表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/banner")
@Api(tags="店铺横幅管理")
public class BannerController {

    private final BannerService bannerService;

    @GetMapping("/list")
    @ApiOperation("分页")
    public ObjectResult<PageResult<BannerEntity>> list(
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        Page<BannerEntity> page = bannerService.listBannerByPage(params, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(page));
    }

    @GetMapping("/info/{id}")
    @ApiOperation("详情")
    public ObjectResult<BannerEntity> getById(@PathVariable("id") Long id){
        BannerEntity data = bannerService.getById(id);
        return ObjectResult.success(data);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ObjectResult<String> save(@Validated @RequestBody BannerEntity entity){
        bannerService.save(entity);
        return ObjectResult.success();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public ObjectResult<String> update(@Validated @RequestBody BannerEntity entity){
        bannerService.updateById(entity);
        return ObjectResult.success();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public ObjectResult<String> delete(@RequestBody Long[] ids){
        bannerService.removeByIds(Arrays.asList(ids));
        return ObjectResult.success();
    }

}