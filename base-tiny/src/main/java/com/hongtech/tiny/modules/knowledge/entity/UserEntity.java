package com.hongtech.tiny.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Data
@TableName("tb_user")
public class UserEntity implements Serializable {

    /**
     * 主键
     */
	private Long id;

    /**
     * 用户名
     */
	private String username;

    /**
     * 手机号
     */
	private String mobile;

    /**
     * 密码
     */
	private String password;

    /**
     * 头像
     */
	private String headPic;

    /**
     * 性别 0 未知 1男 2 女
     */
	private Integer gender;

    /**
     * 简介
     */
	private String introduction;

	/**
	 * 生日
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
	private Date birthday;

	/**
	 * 认证状态
	 */
	private Integer authStatus;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

}