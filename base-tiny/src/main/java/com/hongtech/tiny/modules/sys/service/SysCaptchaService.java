package com.hongtech.tiny.modules.sys.service;

/**
 * 验证码
 */
public interface SysCaptchaService {

    /**
     * 获取图片验证码
     *
     * @param uuid 全局唯一ID
     */
    String getCaptcha(String uuid);

    /**
     * 验证码效验
     *
     * @param uuid 全局唯一ID
     * @param code 验证码
     */
    boolean validate(String uuid, String code);

}
