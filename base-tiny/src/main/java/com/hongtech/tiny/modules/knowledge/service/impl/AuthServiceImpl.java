package com.hongtech.tiny.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.knowledge.mapper.AuthMapper;
import com.hongtech.tiny.modules.knowledge.entity.AuthEntity;
import com.hongtech.tiny.modules.knowledge.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 认证信息
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-12
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl extends ServiceImpl<AuthMapper, AuthEntity> implements AuthService {

    @Override
    public Page<AuthEntity> listAuthByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<AuthEntity> page = new Page(pageNum, pageSize);
        return baseMapper.listAuthByPage(page, params);
    }

    @Override
    public AuthEntity getUserAuthByUserId(Long userId) {
        LambdaQueryWrapper<AuthEntity> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(AuthEntity::getUserId, userId);
        return baseMapper.selectOne(queryWrapper);
    }
}