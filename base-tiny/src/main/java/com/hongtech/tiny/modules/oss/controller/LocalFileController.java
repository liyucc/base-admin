package com.hongtech.tiny.modules.oss.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.exception.CustomException;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.oss.entity.LocalFileEntity;
import com.hongtech.tiny.modules.oss.service.LocalFileService;
import com.hongtech.tiny.modules.oss.utils.FileUtils;
import com.hongtech.tiny.modules.oss.vo.LocalFileQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@Api(tags = "本地文件存储")
@RequestMapping("/local/file")
public class LocalFileController {

    private final LocalFileService localFileService;

    @ApiOperation("查询文件")
    @GetMapping(value = "/list")
    public PageResult<LocalFileEntity> queryFile(LocalFileQueryCriteria criteria, Page<LocalFileEntity> page) {
        return localFileService.queryAll(criteria, page);
    }

    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void exportFile(HttpServletResponse response, LocalFileQueryCriteria criteria) throws IOException {
        localFileService.download(localFileService.queryAll(criteria), response);
    }

    @ApiOperation("上传文件")
    @PostMapping(value = "/upload")
    public ResponseEntity<Object> createFile(@RequestParam String name, @RequestParam("file") MultipartFile file) {
        LocalFileEntity localFileEntity = localFileService.create(name, file);
        return new ResponseEntity<>(localFileEntity, HttpStatus.OK);
    }

    @ApiOperation("上传图片")
    @PostMapping("/pictures")
    public ResponseEntity<LocalFileEntity> uploadPicture(@RequestParam MultipartFile file) {
        // 判断文件是否为图片
        String suffix = FileUtils.getExtensionName(file.getOriginalFilename());
        if (!FileUtils.IMAGE.equals(FileUtils.getFileType(suffix))) {
            throw new CustomException("只能上传图片");
        }
        LocalFileEntity localFileEntity = localFileService.create(null, file);
        return new ResponseEntity<>(localFileEntity, HttpStatus.OK);
    }

    @ApiOperation("修改文件")
    @PostMapping("/update")
    public ResponseEntity<Object> updateFile(@Validated @RequestBody LocalFileEntity resources) {
        localFileService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation("多选删除")
    @PostMapping("/delete")
    public ResponseEntity<Object> deleteFile(@RequestBody Long[] ids) {
        localFileService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}