package com.hongtech.tiny.modules.knowledge.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hongtech.tiny.modules.knowledge.entity.AuthEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 认证信息
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-12
 */
@Mapper
public interface AuthMapper extends BaseMapper<AuthEntity> {

    Page<AuthEntity> listAuthByPage(Page<AuthEntity> page, @Param("query" ) Map<String, Object> params);
	
}