package com.hongtech.tiny.modules.oss.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.common.exception.CustomException;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.oss.entity.QiniuConfigEntity;
import com.hongtech.tiny.modules.oss.entity.QiniuFileEntity;
import com.hongtech.tiny.modules.oss.mapper.QiniuFileMapper;
import com.hongtech.tiny.modules.oss.service.QiniuFileService;
import com.hongtech.tiny.modules.oss.utils.FileUtils;
import com.hongtech.tiny.modules.oss.utils.QiNiuUtil;
import com.hongtech.tiny.modules.oss.vo.QiniuFileQueryCriteria;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.util.Auth;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = "qiNiu")
public class QiniuFileServiceImpl extends ServiceImpl<QiniuFileMapper, QiniuFileEntity> implements QiniuFileService {

    private final QiniuFileMapper qiniuFileMapper;

    @Value("${qiniu.max-size}")
    private Long maxSize;

    @Override
    public PageResult<QiniuFileEntity> queryAll(QiniuFileQueryCriteria criteria, Page<QiniuFileEntity> page){
        Page<QiniuFileEntity> list = qiniuFileMapper.findAll(criteria, page);
        return PageResult.restPage(list);
    }

    @Override
    public List<QiniuFileEntity> queryAll(QiniuFileQueryCriteria criteria) {
        return qiniuFileMapper.findAll(criteria);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public QiniuFileEntity upload(MultipartFile file, QiniuConfigEntity qiniuConfigEntity) {
        FileUtils.checkSize(maxSize, file.getSize());
        if(qiniuConfigEntity.getId() == null){
            throw new CustomException("请先添加相应配置，再操作");
        }
        // 构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(QiNiuUtil.getRegion(qiniuConfigEntity.getZone()));
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(qiniuConfigEntity.getAccessKey(), qiniuConfigEntity.getSecretKey());
        String upToken = auth.uploadToken(qiniuConfigEntity.getBucket());
        try {
            String key = file.getOriginalFilename();
            if(qiniuFileMapper.findByKey(key) != null) {
                key = QiNiuUtil.getKey(key);
            }
            Response response = uploadManager.put(file.getBytes(), key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
            QiniuFileEntity content = qiniuFileMapper.findByKey(FileUtils.getFileNameNoEx(putRet.key));
            if(content == null){
                //存入数据库
                QiniuFileEntity qiniuFileEntity = new QiniuFileEntity();
                qiniuFileEntity.setSuffix(FileUtils.getExtensionName(putRet.key));
                qiniuFileEntity.setBucket(qiniuConfigEntity.getBucket());
                qiniuFileEntity.setType(qiniuConfigEntity.getType());
                qiniuFileEntity.setKey(FileUtils.getFileNameNoEx(putRet.key));
                qiniuFileEntity.setUrl(qiniuConfigEntity.getHost() + "/" + putRet.key);
                qiniuFileEntity.setSize(FileUtils.getSize(Integer.parseInt(String.valueOf(file.getSize()))));
                save(qiniuFileEntity);
            }
            return content;
        } catch (Exception e) {
           throw new CustomException(e.getMessage());
        }
    }

    @Override
    public String download(QiniuFileEntity content, QiniuConfigEntity config){
        String finalUrl;
        String type = "公开";
        if(type.equals(content.getType())){
            finalUrl  = content.getUrl();
        } else {
            Auth auth = Auth.create(config.getAccessKey(), config.getSecretKey());
            // 1小时，可以自定义链接过期时间
            long expireInSeconds = 3600;
            finalUrl = auth.privateDownloadUrl(content.getUrl(), expireInSeconds);
        }
        return finalUrl;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(QiniuFileEntity content, QiniuConfigEntity config) {
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(QiNiuUtil.getRegion(config.getZone()));
        Auth auth = Auth.create(config.getAccessKey(), config.getSecretKey());
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(content.getBucket(), content.getKey() + "." + content.getSuffix());
        } catch (QiniuException ex) {
            ex.printStackTrace();
        } finally {
            removeById(content);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void synchronize(QiniuConfigEntity config) {
        if(config.getId() == null){
            throw new CustomException("请先添加相应配置，再操作");
        }
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(QiNiuUtil.getRegion(config.getZone()));
        Auth auth = Auth.create(config.getAccessKey(), config.getSecretKey());
        BucketManager bucketManager = new BucketManager(auth, cfg);
        //文件名前缀
        String prefix = "";
        //每次迭代的长度限制，最大1000，推荐值 1000
        int limit = 1000;
        //指定目录分隔符，列出所有公共前缀（模拟列出目录效果）。缺省值为空字符串
        String delimiter = "";
        //列举空间文件列表
        BucketManager.FileListIterator fileListIterator = bucketManager.createFileListIterator(config.getBucket(), prefix, limit, delimiter);
        while (fileListIterator.hasNext()) {
            //处理获取的file list结果
            QiniuFileEntity qiniuFileEntity;
            FileInfo[] items = fileListIterator.next();
            for (FileInfo item : items) {
                if(qiniuFileMapper.findByKey(FileUtils.getFileNameNoEx(item.key)) == null){
                    qiniuFileEntity = new QiniuFileEntity();
                    qiniuFileEntity.setSize(FileUtils.getSize(Integer.parseInt(String.valueOf(item.fsize))));
                    qiniuFileEntity.setSuffix(FileUtils.getExtensionName(item.key));
                    qiniuFileEntity.setKey(FileUtils.getFileNameNoEx(item.key));
                    qiniuFileEntity.setType(config.getType());
                    qiniuFileEntity.setBucket(config.getBucket());
                    qiniuFileEntity.setUrl(config.getHost()+"/"+item.key);
                    save(qiniuFileEntity);
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAll(Long[] ids, QiniuConfigEntity config) {
        for (Long id : ids) {
            delete(getById(id), config);
        }
    }

    @Override
    public void downloadList(List<QiniuFileEntity> queryAll, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (QiniuFileEntity content : queryAll) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("文件名", content.getKey());
            map.put("文件类型", content.getSuffix());
            map.put("空间名称", content.getBucket());
            map.put("文件大小", content.getSize());
            map.put("空间类型", content.getType());
            map.put("创建日期", content.getUpdateTime());
            list.add(map);
        }
        FileUtils.downloadExcel(list, response);
    }
}
