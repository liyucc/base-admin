package com.hongtech.tiny.modules.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.sys.entity.SysResource;
import com.hongtech.tiny.modules.sys.service.SysResourceService;
import com.hongtech.tiny.security.component.DynamicSecurityMetadataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 后台资源管理Controller
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "后台资源管理")
@RequestMapping("/resource")
public class SysResourceController {

    private final SysResourceService resourceService;

    private final DynamicSecurityMetadataSource dynamicSecurityMetadataSource;

    @ApiOperation("添加后台资源")
    @PostMapping(value = "/create")
    public ObjectResult<Object> create(@Validated @RequestBody SysResource sysResource) {
        boolean success = resourceService.create(sysResource);
        dynamicSecurityMetadataSource.clearDataSource();
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("修改后台资源")
    @PostMapping(value = "/update/{id}")
    public ObjectResult<Object> update(
            @PathVariable Long id,
            @Validated @RequestBody SysResource sysResource) {
        boolean success = resourceService.update(id, sysResource);
        dynamicSecurityMetadataSource.clearDataSource();
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("根据ID获取资源详情")
    @GetMapping(value = "/{id}")
    public ObjectResult<SysResource> getItem(@PathVariable Long id) {
        SysResource sysResource = resourceService.getById(id);
        return ObjectResult.success(sysResource);
    }

    @ApiOperation("根据ID删除后台资源")
    @PostMapping(value = "/delete/{id}")
    public ObjectResult<Object> delete(@PathVariable Long id) {
        boolean success = resourceService.delete(id);
        dynamicSecurityMetadataSource.clearDataSource();
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("分页模糊查询后台资源")
    @GetMapping(value = "/list")
    public ObjectResult<PageResult<SysResource>> list(
            @RequestParam(required = false) Long categoryId,
            @RequestParam(required = false) String nameKeyword,
            @RequestParam(required = false) String urlKeyword,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<SysResource> resourceList = resourceService.list(categoryId, nameKeyword, urlKeyword, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(resourceList));
    }

    @ApiOperation("查询所有后台资源")
    @GetMapping(value = "/listAll")
    public ObjectResult<List<SysResource>> listAll() {
        List<SysResource> resourceList = resourceService.list();
        return ObjectResult.success(resourceList);
    }

}
