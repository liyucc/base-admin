package com.hongtech.tiny.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 认证信息
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-12
 */
@Data
@TableName("tb_auth")
public class AuthEntity implements Serializable {

    /**
     * 主键
     */
	private Long id;

    /**
     * 用户ID
     */
	private Long userId;

    /**
     * 身份证号码
     */
	private String idCard;

    /**
     * 城市ID
     */
	private Long cityId;

    /**
     * 城市名称
     */
	private String cityName;

    /**
     * 身份证正面
     */
	private String frontPic;

    /**
     * 身份证反面
     */
	private String backPic;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

}