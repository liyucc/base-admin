package com.hongtech.tiny.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.modules.oss.entity.QiniuFileEntity;
import com.hongtech.tiny.modules.oss.vo.QiniuFileQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QiniuFileMapper extends BaseMapper<QiniuFileEntity> {

    QiniuFileEntity findByKey(@Param("name") String name);

    Page<QiniuFileEntity> findAll(@Param("criteria") QiniuFileQueryCriteria criteria, Page<QiniuFileEntity> page);

    List<QiniuFileEntity> findAll(QiniuFileQueryCriteria criteria);

}
