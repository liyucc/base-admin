package com.hongtech.tiny.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.sys.mapper.SysRoleMenuRelationMapper;
import com.hongtech.tiny.modules.sys.service.SysRoleMenuRelationService;
import com.hongtech.tiny.modules.sys.entity.SysRoleMenuRelation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 角色菜单关系管理Service实现类
 */
@Service
@RequiredArgsConstructor
public class SysRoleMenuRelationServiceImpl extends ServiceImpl<SysRoleMenuRelationMapper, SysRoleMenuRelation> implements SysRoleMenuRelationService {

}
