package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysRoleResourceRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台角色资源关系表 Mapper 接口
 * </p>
 */
public interface SysRoleResourceRelationMapper extends BaseMapper<SysRoleResourceRelation> {

}
