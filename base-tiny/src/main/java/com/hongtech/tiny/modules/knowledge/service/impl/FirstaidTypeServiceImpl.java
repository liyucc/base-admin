package com.hongtech.tiny.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.knowledge.mapper.FirstaidTypeMapper;
import com.hongtech.tiny.modules.knowledge.entity.FirstaidTypeEntity;
import com.hongtech.tiny.modules.knowledge.service.FirstaidTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 急救类型表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@Service
@RequiredArgsConstructor
public class FirstaidTypeServiceImpl extends ServiceImpl<FirstaidTypeMapper, FirstaidTypeEntity> implements FirstaidTypeService {

    @Override
    public Page<FirstaidTypeEntity> listFirstaidTypeByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<FirstaidTypeEntity> page = new Page(pageNum, pageSize);
        return baseMapper.listFirstaidTypeByPage(page, params);
    }

}