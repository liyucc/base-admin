package com.hongtech.tiny.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.sys.service.SysRoleResourceRelationService;
import com.hongtech.tiny.modules.sys.mapper.SysRoleResourceRelationMapper;
import com.hongtech.tiny.modules.sys.entity.SysRoleResourceRelation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 角色资源关系管理Service实现类
 */
@Service
@RequiredArgsConstructor
public class SysRoleResourceRelationServiceImpl extends ServiceImpl<SysRoleResourceRelationMapper, SysRoleResourceRelation> implements SysRoleResourceRelationService {

}
