package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysRoleMenuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台角色菜单关系表 Mapper 接口
 * </p>
 */
public interface SysRoleMenuRelationMapper extends BaseMapper<SysRoleMenuRelation> {

}
