package com.hongtech.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.sys.entity.SysAdminRoleRelation;

/**
 * 管理员角色关系管理Service
 */
public interface SysAdminRoleRelationService extends IService<SysAdminRoleRelation> {

}
