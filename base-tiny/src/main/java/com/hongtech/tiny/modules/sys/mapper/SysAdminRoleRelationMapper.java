package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysAdminRoleRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台用户和角色关系表 Mapper 接口
 * </p>
 */
public interface SysAdminRoleRelationMapper extends BaseMapper<SysAdminRoleRelation> {

}
