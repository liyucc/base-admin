package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 后台菜单表 Mapper 接口
 * </p>
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据后台用户ID获取菜单
     */
    List<SysMenu> getMenuList(@Param("adminId") Long adminId);
    /**
     * 根据角色ID获取菜单
     */
    List<SysMenu> getMenuListByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据用户ID获取菜单列表
     */
    List<SysMenu> listMenuTreeByUserId(Long id);

}
