package com.hongtech.tiny.modules.knowledge.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hongtech.tiny.modules.knowledge.entity.FirstAidEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 急救知识表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@Mapper
public interface FirstAidMapper extends BaseMapper<FirstAidEntity> {

    Page<FirstAidEntity> listFirstAidByPage(Page<FirstAidEntity> page, @Param("query" ) Map<String, Object> params);
	
}