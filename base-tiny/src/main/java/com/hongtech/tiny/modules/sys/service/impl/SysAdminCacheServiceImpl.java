package com.hongtech.tiny.modules.sys.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hongtech.tiny.common.service.RedisService;
import com.hongtech.tiny.modules.sys.entity.SysAdmin;
import com.hongtech.tiny.modules.sys.entity.SysAdminRoleRelation;
import com.hongtech.tiny.modules.sys.entity.SysResource;
import com.hongtech.tiny.modules.sys.mapper.SysAdminMapper;
import com.hongtech.tiny.modules.sys.service.SysAdminCacheService;
import com.hongtech.tiny.modules.sys.service.SysAdminRoleRelationService;
import com.hongtech.tiny.modules.sys.service.SysAdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 后台用户缓存管理Service实现类
 */
@Service
@RequiredArgsConstructor
public class SysAdminCacheServiceImpl implements SysAdminCacheService {

    private final SysAdminService adminService;

    private final RedisService redisService;

    private final SysAdminMapper adminMapper;

    private final SysAdminRoleRelationService adminRoleRelationService;

    @Value("${redis.database}")
    private String redisDatabase;

    @Value("${redis.expire.common}")
    private Long redisExpire;

    @Value("${redis.key.admin}")
    private String redisKeyAdmin;

    @Value("${redis.key.resourceList}")
    private String redisKeyResourceList;

    @Override
    public void delAdmin(Long adminId) {
        SysAdmin admin = adminService.getById(adminId);
        if (admin != null) {
            String key = redisDatabase + ":" + redisKeyAdmin + ":" + admin.getUsername();
            redisService.del(key);
        }
    }

    @Override
    public void delResourceList(Long adminId) {
        String key = redisDatabase + ":" + redisKeyResourceList + ":" + adminId;
        redisService.del(key);
    }

    @Override
    public void delResourceListByRoleIds(List<Long> roleIds) {
        QueryWrapper<SysAdminRoleRelation> wrapper = new QueryWrapper<>();
        wrapper.lambda().in(SysAdminRoleRelation::getRoleId, roleIds);
        List<SysAdminRoleRelation> relationList = adminRoleRelationService.list(wrapper);
        if (CollUtil.isNotEmpty(relationList)) {
            String keyPrefix = redisDatabase + ":" + redisKeyResourceList + ":" ;
            List<String> keys = relationList.stream().map(relation -> keyPrefix + relation.getAdminId()).collect(Collectors.toList());
            redisService.del(keys);
        }
    }

    @Override
    public void delResourceListByResource(Long resourceId) {
        List<Long> adminIdList = adminMapper.getAdminIdList(resourceId);
        if (CollUtil.isNotEmpty(adminIdList)) {
            String keyPrefix = redisDatabase + ":" + redisKeyResourceList + ":" ;
            List<String> keys = adminIdList.stream().map(adminId -> keyPrefix + adminId).collect(Collectors.toList());
            redisService.del(keys);
        }
    }

    @Override
    public SysAdmin getAdmin(String username) {
        String key = redisDatabase + ":" + redisKeyAdmin + ":" + username;
        return (SysAdmin) redisService.get(key);
    }

    @Override
    public void setAdmin(SysAdmin admin) {
        String key = redisDatabase + ":" + redisKeyAdmin + ":" + admin.getUsername();
        redisService.set(key, admin, redisExpire);
    }

    @Override
    public List<SysResource> getResourceList(Long adminId) {
        String key = redisDatabase + ":" + redisKeyResourceList + ":" + adminId;
        return (List<SysResource>) redisService.get(key);
    }

    @Override
    public void setResourceList(Long adminId, List<SysResource> resourceList) {
        String key = redisDatabase + ":" + redisKeyResourceList + ":" + adminId;
        redisService.set(key, resourceList, redisExpire);
    }

}
