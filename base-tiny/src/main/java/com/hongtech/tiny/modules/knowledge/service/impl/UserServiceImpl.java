package com.hongtech.tiny.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.knowledge.mapper.UserMapper;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;
import com.hongtech.tiny.modules.knowledge.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 用户
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Override
    public Page<UserEntity> listUserByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<UserEntity> page = new Page<>(pageNum, pageSize);
        return baseMapper.listUserByPage(page, params);
    }

    @Override
    public UserEntity getUserInfoByMobile(String mobile) {
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserEntity::getMobile, mobile);
        return baseMapper.selectOne(queryWrapper);
    }

}