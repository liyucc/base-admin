package com.hongtech.tiny.modules.knowledge.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;
import com.hongtech.tiny.modules.knowledge.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.Map;


/**
 * 用户
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Api(tags="店铺用户管理")
public class UserController {

    private final UserService userService;

    @GetMapping("/list")
    @ApiOperation("分页")
    public ObjectResult<PageResult<UserEntity>> list(
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        Page<UserEntity> page = userService.listUserByPage(params, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(page));
    }

    @GetMapping("/info/{id}")
    @ApiOperation("详情")
    public ObjectResult<UserEntity> getById(@PathVariable("id") Long id){
        UserEntity data = userService.getById(id);
        return ObjectResult.success(data);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ObjectResult<String> save(@Validated @RequestBody UserEntity entity){
        userService.save(entity);
        return ObjectResult.success();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public ObjectResult<String> update(@Validated @RequestBody UserEntity entity){
        userService.updateById(entity);
        return ObjectResult.success();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public ObjectResult<String> delete(@RequestBody Long[] ids){
        userService.removeByIds(Arrays.asList(ids));
        return ObjectResult.success();
    }

}