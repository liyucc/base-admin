package com.hongtech.tiny.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.modules.oss.entity.LocalFileEntity;
import com.hongtech.tiny.modules.oss.vo.LocalFileQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LocalFileMapper extends BaseMapper<LocalFileEntity> {

    Page<LocalFileEntity> findAll(@Param("criteria") LocalFileQueryCriteria criteria, Page<LocalFileEntity> page);

    List<LocalFileEntity> findAll(@Param("criteria") LocalFileQueryCriteria criteria);

}
