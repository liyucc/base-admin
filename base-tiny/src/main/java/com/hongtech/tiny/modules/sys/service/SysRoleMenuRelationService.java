package com.hongtech.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.sys.entity.SysRoleMenuRelation;

/**
 * 角色菜单关系管理Service
 */
public interface SysRoleMenuRelationService extends IService<SysRoleMenuRelation> {

}
