package com.hongtech.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.sys.entity.SysRoleResourceRelation;

/**
 * 角色资源关系管理Service
 */
public interface SysRoleResourceRelationService extends IService<SysRoleResourceRelation> {
}
