package com.hongtech.tiny.modules.sys.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hongtech.tiny.modules.sys.entity.RegionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 地区表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-16
 */
@Mapper
public interface RegionMapper extends BaseMapper<RegionEntity> {

    Page<RegionEntity> listRegionByPage(Page<RegionEntity> page, @Param("query" ) Map<String, Object> params);
	
}