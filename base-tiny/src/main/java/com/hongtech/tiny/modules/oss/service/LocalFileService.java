package com.hongtech.tiny.modules.oss.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.oss.entity.LocalFileEntity;
import com.hongtech.tiny.modules.oss.vo.LocalFileQueryCriteria;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface LocalFileService extends IService<LocalFileEntity> {

    /**
     * 分页查询
     *
     * @param criteria 条件
     * @param page     分页参数
     * @return /
     */
    PageResult<LocalFileEntity> queryAll(LocalFileQueryCriteria criteria, Page<LocalFileEntity> page);

    /**
     * 查询全部数据
     * @param criteria 条件
     * @return /
     */
    List<LocalFileEntity> queryAll(LocalFileQueryCriteria criteria);

    /**
     * 上传
     * @param name 文件名称
     * @param file 文件
     * @return /
     */
    LocalFileEntity create(String name, MultipartFile file);

    /**
     * 编辑
     * @param resources 文件信息
     */
    void update(LocalFileEntity resources);

    /**
     * 多选删除
     * @param ids /
     */
    void deleteAll(Long[] ids);

    /**
     * 导出数据
     * @param localFileEntities 待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<LocalFileEntity> localFileEntities, HttpServletResponse response) throws IOException;

}