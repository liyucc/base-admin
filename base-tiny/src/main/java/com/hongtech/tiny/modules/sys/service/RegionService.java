package com.hongtech.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.sys.entity.RegionEntity;

import java.util.List;
import java.util.Map;

/**
 * 地区表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-16
 */
public interface RegionService extends IService<RegionEntity> {

    Page<RegionEntity> listRegionByPage(Map<String, Object> params, Integer pageSize, Integer pageNum);

    List<RegionEntity> getProvinceList();

    List<RegionEntity> getCityListByProvinceId(String provinceId);

    List<RegionEntity> listRegionByParentId(Long parentId);

}