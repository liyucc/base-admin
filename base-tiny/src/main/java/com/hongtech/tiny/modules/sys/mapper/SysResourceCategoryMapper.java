package com.hongtech.tiny.modules.sys.mapper;

import com.hongtech.tiny.modules.sys.entity.SysResourceCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源分类表 Mapper 接口
 * </p>
 */
public interface SysResourceCategoryMapper extends BaseMapper<SysResourceCategory> {

}
