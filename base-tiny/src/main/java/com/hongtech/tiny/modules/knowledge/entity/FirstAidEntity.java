package com.hongtech.tiny.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 急救知识表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@Data
@TableName("tb_first_aid")
public class FirstAidEntity implements Serializable {


    /**
     * 
     */
	private Long id;

    /**
     * 类型ID
     */
	private Long typeId;

    /**
     * 类型名称
     */
	private String typeName;

    /**
     * 急救名称
     */
	private String name;

    /**
     * 急救内容
     */
	private String content;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

}