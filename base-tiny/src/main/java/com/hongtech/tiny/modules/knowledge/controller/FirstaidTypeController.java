package com.hongtech.tiny.modules.knowledge.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.knowledge.entity.FirstaidTypeEntity;
import com.hongtech.tiny.modules.knowledge.service.FirstaidTypeService;
import lombok.RequiredArgsConstructor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.Map;

/**
 * 急救类型表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/firstaidtype")
@Api(tags="急救类型表")
public class FirstaidTypeController {

    private final FirstaidTypeService firstaidTypeService;

    @GetMapping("/list")
    @ApiOperation("分页")
    public ObjectResult<PageResult<FirstaidTypeEntity>> list(
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        Page<FirstaidTypeEntity> page = firstaidTypeService.listFirstaidTypeByPage(params, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(page));
    }

    @GetMapping("/info/{id}")
    @ApiOperation("详情")
    public ObjectResult<FirstaidTypeEntity> getById(@PathVariable("id") Long id){
        FirstaidTypeEntity data = firstaidTypeService.getById(id);
        return ObjectResult.success(data);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ObjectResult<String> save(@Validated @RequestBody FirstaidTypeEntity entity){
        firstaidTypeService.save(entity);
        return ObjectResult.success();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public ObjectResult<String> update(@Validated @RequestBody FirstaidTypeEntity entity){
        firstaidTypeService.updateById(entity);
        return ObjectResult.success();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public ObjectResult<String> delete(@RequestBody Long[] ids){
        firstaidTypeService.removeByIds(Arrays.asList(ids));
        return ObjectResult.success();
    }

}