package com.hongtech.tiny.modules.knowledge.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 用户
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    Page<UserEntity> listUserByPage(Page<UserEntity> page, @Param("query" ) Map<String, Object> params);
	
}