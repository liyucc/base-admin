package com.hongtech.tiny.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.knowledge.mapper.FirstAidMapper;
import com.hongtech.tiny.modules.knowledge.entity.FirstAidEntity;
import com.hongtech.tiny.modules.knowledge.service.FirstAidService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 急救知识表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@Service
@RequiredArgsConstructor
public class FirstAidServiceImpl extends ServiceImpl<FirstAidMapper, FirstAidEntity> implements FirstAidService {

    @Override
    public Page<FirstAidEntity> listFirstAidByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<FirstAidEntity> page = new Page(pageNum, pageSize);
        return baseMapper.listFirstAidByPage(page, params);
    }

}