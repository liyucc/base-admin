package com.hongtech.tiny.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.sys.service.SysAdminRoleRelationService;
import com.hongtech.tiny.modules.sys.mapper.SysAdminRoleRelationMapper;
import com.hongtech.tiny.modules.sys.entity.SysAdminRoleRelation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 管理员角色关系管理Service实现类
 */
@Service
@RequiredArgsConstructor
public class SysAdminRoleRelationServiceImpl extends ServiceImpl<SysAdminRoleRelationMapper, SysAdminRoleRelation> implements SysAdminRoleRelationService {

}
