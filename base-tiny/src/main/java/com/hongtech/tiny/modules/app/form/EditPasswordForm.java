package com.hongtech.tiny.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录表单
 */
@Data
@ApiModel(value = "修改密码表单")
public class EditPasswordForm implements Serializable {

    @ApiModelProperty(value = "旧的密码")
    @NotBlank(message="旧的密码不能为空")
    private String oldPassword;

    @ApiModelProperty(value = "新的密码")
    @NotBlank(message="新的密码不能为空")
    private String newPassword;

}
