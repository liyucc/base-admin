package com.hongtech.tiny.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.enums.RegionEnum;
import com.hongtech.tiny.modules.sys.mapper.RegionMapper;
import com.hongtech.tiny.modules.sys.entity.RegionEntity;
import com.hongtech.tiny.modules.sys.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 地区表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-16
 */
@Service
@RequiredArgsConstructor
public class RegionServiceImpl extends ServiceImpl<RegionMapper, RegionEntity> implements RegionService {

    @Override
    public Page<RegionEntity> listRegionByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<RegionEntity> page = new Page(pageNum, pageSize);
        return baseMapper.listRegionByPage(page, params);
    }

    @Override
    public List<RegionEntity> getProvinceList() {
        LambdaQueryWrapper<RegionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RegionEntity::getLevel, RegionEnum.LEVEL_PROVINCE.getCode());
        queryWrapper.orderByAsc(RegionEntity::getId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<RegionEntity> getCityListByProvinceId(String provinceId) {
        LambdaQueryWrapper<RegionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RegionEntity::getParentId, provinceId);
        queryWrapper.eq(RegionEntity::getLevel, RegionEnum.LEVEL_CITY.getCode());
        queryWrapper.orderByAsc(RegionEntity::getId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<RegionEntity> listRegionByParentId(Long parentId) {
        LambdaQueryWrapper<RegionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RegionEntity::getParentId, parentId);
        queryWrapper.orderByAsc(RegionEntity::getId);
        return baseMapper.selectList(queryWrapper);
    }
}