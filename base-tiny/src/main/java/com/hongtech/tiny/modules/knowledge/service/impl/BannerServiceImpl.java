package com.hongtech.tiny.modules.knowledge.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hongtech.tiny.modules.knowledge.entity.BannerEntity;
import com.hongtech.tiny.modules.knowledge.mapper.BannerMapper;
import com.hongtech.tiny.modules.knowledge.service.BannerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 横幅表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Service
@RequiredArgsConstructor
public class BannerServiceImpl extends ServiceImpl<BannerMapper, BannerEntity> implements BannerService {

    /**
     * 根据位置查询Banner列表
     * @param code 显示位置
     */
    @Override
    public List<BannerEntity> listBannerByPosition(Integer code) {
        LambdaQueryWrapper<BannerEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BannerEntity::getPosition, code);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public Page<BannerEntity> listBannerByPage(Map<String, Object> params, Integer pageSize, Integer pageNum) {
        Page<BannerEntity> page = new Page<>(pageNum, pageSize);
        return baseMapper.listBannerByPage(page, params);
    }

}