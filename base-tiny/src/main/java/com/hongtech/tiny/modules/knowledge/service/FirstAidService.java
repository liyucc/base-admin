package com.hongtech.tiny.modules.knowledge.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.knowledge.entity.FirstAidEntity;

import java.util.Map;

/**
 * 急救知识表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
public interface FirstAidService extends IService<FirstAidEntity> {

    Page<FirstAidEntity> listFirstAidByPage(Map<String, Object> params, Integer pageSize, Integer pageNum);

}