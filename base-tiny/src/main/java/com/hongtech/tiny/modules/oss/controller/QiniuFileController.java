package com.hongtech.tiny.modules.oss.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.oss.entity.QiniuConfigEntity;
import com.hongtech.tiny.modules.oss.entity.QiniuFileEntity;
import com.hongtech.tiny.modules.oss.service.QiNiuConfigService;
import com.hongtech.tiny.modules.oss.service.QiniuFileService;
import com.hongtech.tiny.modules.oss.vo.QiniuFileQueryCriteria;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/qiniu/file")
@Api(tags = "七牛云端存储")
public class QiniuFileController {

    private final QiniuFileService qiniuFileService;

    private final QiNiuConfigService qiNiuConfigService;

    @GetMapping(value = "/config")
    public ResponseEntity<QiniuConfigEntity> queryQiNiuConfig() {
        return new ResponseEntity<>(qiNiuConfigService.getConfig(), HttpStatus.OK);
    }

    @ApiOperation("配置七牛云存储")
    @PostMapping(value = "/config")
    public ResponseEntity<Object> updateQiNiuConfig(@Validated @RequestBody QiniuConfigEntity qiniuConfigEntity) {
        qiNiuConfigService.saveConfig(qiniuConfigEntity);
        qiNiuConfigService.updateType(qiniuConfigEntity.getType());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void exportQiNiu(HttpServletResponse response, QiniuFileQueryCriteria criteria) throws IOException {
        qiniuFileService.downloadList(qiniuFileService.queryAll(criteria), response);
    }

    @ApiOperation("查询文件")
    @GetMapping(value = "/info")
    public PageResult<QiniuFileEntity> queryQiNiu(QiniuFileQueryCriteria criteria, Page<QiniuFileEntity> page) {
        return qiniuFileService.queryAll(criteria, page);
    }

    @ApiOperation("上传文件")
    @PostMapping(value = "/upload")
    public ResponseEntity<Object> uploadQiNiu(@RequestParam MultipartFile file) {
        QiniuFileEntity qiniuFileEntity = qiniuFileService.upload(file, qiNiuConfigService.getConfig());
        Map<String, Object> map = new HashMap<>(3);
        map.put("id", qiniuFileEntity.getId());
        map.put("errno", 0);
        map.put("data", new String[]{qiniuFileEntity.getUrl()});
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @ApiOperation("同步七牛云数据")
    @PostMapping(value = "/synchronize")
    public ResponseEntity<Object> synchronizeQiNiu() {
        qiniuFileService.synchronize(qiNiuConfigService.getConfig());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation("下载文件")
    @GetMapping(value = "/download/{id}")
    public ResponseEntity<Object> downloadQiNiu(@PathVariable Long id) {
        Map<String, Object> map = new HashMap<>(1);
        map.put("url", qiniuFileService.download(qiniuFileService.getById(id), qiNiuConfigService.getConfig()));
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @ApiOperation("删除文件")
    @GetMapping(value = "/delete/{id}")
    public ResponseEntity<Object> deleteQiNiu(@PathVariable Long id) {
        qiniuFileService.delete(qiniuFileService.getById(id), qiNiuConfigService.getConfig());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation("删除多张图片")
    @PostMapping(value = "/deletes")
    public ResponseEntity<Object> deleteAllQiNiu(@RequestBody Long[] ids) {
        qiniuFileService.deleteAll(ids, qiNiuConfigService.getConfig());
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
