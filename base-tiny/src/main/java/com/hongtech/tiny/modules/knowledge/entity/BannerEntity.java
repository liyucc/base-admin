package com.hongtech.tiny.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 横幅表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
@Data
@TableName("tb_banner")
public class BannerEntity implements Serializable {


    /**
     * 主键
     */
	private Long id;

    /**
     * 标题
     */
	private String title;

    /**
     * 跳转链接
     */
	private String linkUrl;

    /**
     * 展示图片
     */
	private String picUrl;

    /**
     * 展示位置：1则是首页
     */
	private Integer position;

    /**
     * 活动内容
     */
	private String content;

	/**
	 * 展示开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	private Date startTime;

	/**
	 * 展示结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	private Date endTime;

    /**
     * 是否启用
     */
	private Integer enabled;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

}