package com.hongtech.tiny.modules.oss.vo;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class LocalFileQueryCriteria {

    private String blurry;

    private List<Timestamp> createTime;

}