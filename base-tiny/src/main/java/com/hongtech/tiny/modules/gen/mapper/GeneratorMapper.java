package com.hongtech.tiny.modules.gen.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.modules.gen.vo.GenTableVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * MySQL代码生成器
 */
@Mapper
public interface GeneratorMapper {

    Page<GenTableVO> queryList(Page<GenTableVO> page, String tableName);

    Map<String, String> queryTable(String tableName);

    List<Map<String, String>> queryColumns(String tableName);

}
