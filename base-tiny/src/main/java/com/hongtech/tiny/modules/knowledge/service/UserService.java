package com.hongtech.tiny.modules.knowledge.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;

import java.util.Map;

/**
 * 用户
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
public interface UserService extends IService<UserEntity> {

    Page<UserEntity> listUserByPage(Map<String, Object> params, Integer pageSize, Integer pageNum);

    UserEntity getUserInfoByMobile(String mobile);

}