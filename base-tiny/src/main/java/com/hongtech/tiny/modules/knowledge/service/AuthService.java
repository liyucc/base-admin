package com.hongtech.tiny.modules.knowledge.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.knowledge.entity.AuthEntity;

import java.util.Map;

/**
 * 认证信息
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-12
 */
public interface AuthService extends IService<AuthEntity> {

    Page<AuthEntity> listAuthByPage(Map<String, Object> params, Integer pageSize, Integer pageNum);

    AuthEntity getUserAuthByUserId(Long userId);

}