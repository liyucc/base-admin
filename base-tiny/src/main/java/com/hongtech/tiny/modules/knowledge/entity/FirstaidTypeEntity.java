package com.hongtech.tiny.modules.knowledge.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 急救类型表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@Data
@TableName("tb_firstaid_type")
public class FirstaidTypeEntity implements Serializable {


    /**
     * 主键ID
     */
	private Long id;

    /**
     * 类型名称
     */
	private String name;

    /**
     * 显示图片
     */
	private String picUrl;

    /**
     * 排序
     */
	private Integer orderNumber;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

}