package com.hongtech.tiny.modules.oss.vo;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class QiniuFileQueryCriteria {

    private String key;

    private List<Timestamp> createTime;

}
