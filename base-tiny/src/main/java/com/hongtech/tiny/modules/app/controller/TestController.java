package com.hongtech.tiny.modules.app.controller;

import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.modules.app.annotation.Login;
import com.hongtech.tiny.modules.app.annotation.LoginUser;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * APP测试接口
 */
@RestController
@RequestMapping("/app")
@Api(tags = "测试接口")
public class TestController {

    /**
     * 验证@Login @LoginUser
     *
     * @param user 解析当前登录用户信息
     * @return 当前登录用户信息
     */
    @Login
    @GetMapping("userInfo")
    @ApiOperation("获取用户信息")
    public ObjectResult<UserEntity> userInfo(@LoginUser UserEntity user) {
        return ObjectResult.success(user);
    }

    /**
     * 验证@Login
     *
     * @param userId 用户ID
     * @return 登录验证成功返回用户ID
     */
    @Login
    @GetMapping("withToken")
    @ApiOperation("获取用户ID")
    public ObjectResult<String> userInfo() {
        return ObjectResult.success("携带token才能访问！");
    }

    /**
     * 默认忽略验证也能登录
     *
     * @return 无需token也能访问
     */
    @GetMapping("withoutToken")
    @ApiOperation("忽略Token验证")
    public ObjectResult<String> notToken() {
        return ObjectResult.success("无需token也能访问！");
    }

}
