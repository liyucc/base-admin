package com.hongtech.tiny.modules.knowledge.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.knowledge.entity.FirstAidEntity;
import com.hongtech.tiny.modules.knowledge.service.FirstAidService;
import lombok.RequiredArgsConstructor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.Map;


/**
 * 急救知识表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-09
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/firstaid")
@Api(tags="急救知识表")
public class FirstAidController {

    private final FirstAidService firstAidService;

    @GetMapping("/list")
    @ApiOperation("分页")
    public ObjectResult<PageResult<FirstAidEntity>> list(
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum){
        Page<FirstAidEntity> page = firstAidService.listFirstAidByPage(params, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(page));
    }

    @GetMapping("/info/{id}")
    @ApiOperation("详情")
    public ObjectResult<FirstAidEntity> getById(@PathVariable("id") Long id){
        FirstAidEntity data = firstAidService.getById(id);
        return ObjectResult.success(data);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ObjectResult<String> save(@Validated @RequestBody FirstAidEntity entity){
        firstAidService.save(entity);
        return ObjectResult.success();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public ObjectResult<String> update(@Validated @RequestBody FirstAidEntity entity){
        firstAidService.updateById(entity);
        return ObjectResult.success();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public ObjectResult<String> delete(@RequestBody Long[] ids){
        firstAidService.removeByIds(Arrays.asList(ids));
        return ObjectResult.success();
    }

}