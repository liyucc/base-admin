package com.hongtech.tiny.modules.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.sys.entity.RegionEntity;
import com.hongtech.tiny.modules.sys.entity.SysMenu;
import com.hongtech.tiny.modules.sys.service.RegionService;
import lombok.RequiredArgsConstructor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 地区表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2024-03-16
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/region")
@Api(tags="地区表")
public class RegionController {

    private final RegionService regionService;

    @ApiOperation("分页查询后台地区")
    @GetMapping(value = "/listPage/{parentId}")
    public ObjectResult<PageResult<RegionEntity>> listPage(
            @PathVariable Long parentId,
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        params.put("parentId", String.valueOf(parentId));
        Page<RegionEntity> page = regionService.listRegionByPage(params, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(page));
    }

    @ApiOperation("分页查询后台菜单")
    @GetMapping(value = "/list/{parentId}")
    public ObjectResult<List<RegionEntity>> list(@PathVariable Long parentId) {
        List<RegionEntity> regionList = regionService.listRegionByParentId(parentId);
        return ObjectResult.success(regionList);
    }

    @GetMapping("/info/{id}")
    @ApiOperation("详情")
    public ObjectResult<RegionEntity> getById(@PathVariable("id") Long id){
        RegionEntity data = regionService.getById(id);
        return ObjectResult.success(data);
    }

    @PostMapping("/save")
    @ApiOperation("保存")
    public ObjectResult<String> save(@Validated @RequestBody RegionEntity entity){
        regionService.save(entity);
        return ObjectResult.success();
    }

    @PostMapping("/update")
    @ApiOperation("修改")
    public ObjectResult<String> update(@Validated @RequestBody RegionEntity entity){
        regionService.updateById(entity);
        return ObjectResult.success();
    }

    @PostMapping("/delete")
    @ApiOperation("删除")
    public ObjectResult<String> delete(@RequestBody Long[] ids){
        regionService.removeByIds(Arrays.asList(ids));
        return ObjectResult.success();
    }

}