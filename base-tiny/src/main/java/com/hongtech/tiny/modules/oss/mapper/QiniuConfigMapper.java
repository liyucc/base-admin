package com.hongtech.tiny.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hongtech.tiny.modules.oss.entity.QiniuConfigEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QiniuConfigMapper extends BaseMapper<QiniuConfigEntity> {

}
