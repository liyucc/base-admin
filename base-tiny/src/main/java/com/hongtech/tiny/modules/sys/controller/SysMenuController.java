package com.hongtech.tiny.modules.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.sys.dto.SysMenuNode;
import com.hongtech.tiny.modules.sys.entity.SysMenu;
import com.hongtech.tiny.modules.sys.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 后台菜单管理Controller
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "后台菜单管理")
@RequestMapping("/menu")
public class SysMenuController {

    private final SysMenuService menuService;

    @ApiOperation("添加后台菜单")
    @PostMapping(value = "/create")
    public ObjectResult<Object> create(@Validated @RequestBody SysMenu sysMenu) {
        boolean success = menuService.create(sysMenu);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("修改后台菜单")
    @PostMapping(value = "/update/{id}")
    public ObjectResult<Object> update(
            @PathVariable Long id,
            @Validated @RequestBody SysMenu sysMenu) {
        boolean success = menuService.update(id, sysMenu);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("根据ID获取菜单详情")
    @GetMapping(value = "/{id}")
    public ObjectResult<SysMenu> getItem(@PathVariable Long id) {
        SysMenu sysMenu = menuService.getById(id);
        return ObjectResult.success(sysMenu);
    }

    @ApiOperation("根据ID删除后台菜单")
    @PostMapping(value = "/delete/{id}")
    public ObjectResult<Object> delete(@PathVariable Long id) {
        boolean success = menuService.removeById(id);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("分页查询后台菜单")
    @GetMapping(value = "/list/{parentId}")
    public ObjectResult<PageResult<SysMenu>> list(
            @PathVariable Long parentId,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<SysMenu> menuList = menuService.list(parentId, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(menuList));
    }

    @ApiOperation("树形结构返回所有菜单列表")
    @GetMapping(value = "/treeList")
    public ObjectResult<List<SysMenuNode>> treeList() {
        List<SysMenuNode> list = menuService.treeList();
        return ObjectResult.success(list);
    }

    @ApiOperation("修改菜单显示状态")
    @PostMapping(value = "/updateHidden/{id}")
    public ObjectResult<Object> updateHidden(@PathVariable Long id, @RequestParam("hidden") Integer hidden) {
        boolean success = menuService.updateHidden(id, hidden);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

}
