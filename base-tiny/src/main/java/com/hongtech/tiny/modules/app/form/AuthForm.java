package com.hongtech.tiny.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 登录表单
 */
@Data
@ApiModel(value = "认证信息表单")
public class AuthForm implements Serializable {

    @ApiModelProperty(value = "身份证号")
    @NotBlank(message="身份证号")
    private String idCard;

    @ApiModelProperty(value = "城市ID")
    @NotBlank(message="城市ID")
    private Long cityId;

    @ApiModelProperty(value = "城市ID")
    @NotBlank(message="城市ID")
    private String cityName;

    @ApiModelProperty(value = "身份证正面")
    @NotBlank(message="身份证正面")
    private MultipartFile frontPic;

    @ApiModelProperty(value = "身份证反面")
    @NotBlank(message="身份证反面")
    private MultipartFile backPic;

}
