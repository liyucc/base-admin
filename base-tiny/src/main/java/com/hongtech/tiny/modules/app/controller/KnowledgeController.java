package com.hongtech.tiny.modules.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.enums.CommonEnum;
import com.hongtech.tiny.modules.knowledge.entity.BannerEntity;
import com.hongtech.tiny.modules.knowledge.entity.FirstAidEntity;
import com.hongtech.tiny.modules.knowledge.entity.FirstaidTypeEntity;
import com.hongtech.tiny.modules.knowledge.service.BannerService;
import com.hongtech.tiny.modules.knowledge.service.FirstAidService;
import com.hongtech.tiny.modules.knowledge.service.FirstaidTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/app")
@Api(tags = "API接口")
public class KnowledgeController {

    @Autowired
    private BannerService bannerService;

    @Autowired
    private FirstaidTypeService firstaidTypeService;

    @Autowired
    private FirstAidService firstAidService;

    @GetMapping("/banner/list")
    @ApiOperation("横幅列表")
    public ObjectResult<List<BannerEntity>> listBanner() {
        List<BannerEntity> bannerList = bannerService.listBannerByPosition(CommonEnum.HOME_PAGE.getCode());
        return ObjectResult.success(bannerList);
    }

    @GetMapping("/firstAidType/list")
    @ApiOperation("查询急救类别")
    public ObjectResult<List<FirstaidTypeEntity>> listFirstAidType() {
        List<FirstaidTypeEntity> firstAidTypeList = firstaidTypeService.list();
        return ObjectResult.success(firstAidTypeList);
    }

    @GetMapping("/firstAid/page")
    @ApiOperation("分页查询急救知识")
    public ObjectResult<PageResult<FirstAidEntity>> pageFirstAidInfo(
            @ApiIgnore @RequestParam Map<String, Object> params,
            @RequestParam(value = "page", defaultValue = "10") Integer page,
            @RequestParam(value = "size", defaultValue = "1") Integer size){
        Page<FirstAidEntity> pageList = firstAidService.listFirstAidByPage(params, size, page);
        return ObjectResult.success(PageResult.restPage(pageList));
    }

    @GetMapping("/firstAid/detail/{id}")
    @ApiOperation("急救知识详情")
    public ObjectResult<FirstAidEntity> firstAidDetail(@PathVariable Long id) {
        FirstAidEntity firstAid = firstAidService.getById(id);
        return ObjectResult.success(firstAid);
    }

}
