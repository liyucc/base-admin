package com.hongtech.tiny.modules.sys.dto;

import com.hongtech.tiny.modules.sys.entity.SysMenu;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 后台菜单节点封装
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenuNode extends SysMenu {

    @ApiModelProperty(value = "子级菜单")
    private List<SysMenuNode> children;

}
