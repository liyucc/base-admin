package com.hongtech.tiny.modules.knowledge.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.knowledge.entity.BannerEntity;

import java.util.List;
import java.util.Map;

/**
 * 横幅表
 *
 * @author lch 12345678@qq.com
 * @since 1.0.0 2023-10-16
 */
public interface BannerService extends IService<BannerEntity> {

    List<BannerEntity> listBannerByPosition(Integer code);

    Page<BannerEntity> listBannerByPage(Map<String, Object> params, Integer pageSize, Integer pageNum);

}