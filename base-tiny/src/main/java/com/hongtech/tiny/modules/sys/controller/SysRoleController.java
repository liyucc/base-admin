package com.hongtech.tiny.modules.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.common.response.PageResult;
import com.hongtech.tiny.modules.sys.entity.SysMenu;
import com.hongtech.tiny.modules.sys.entity.SysResource;
import com.hongtech.tiny.modules.sys.entity.SysRole;
import com.hongtech.tiny.modules.sys.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 后台用户角色管理
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "后台角色管理")
@RequestMapping("/role")
public class SysRoleController {

    private final SysRoleService roleService;

    @ApiOperation("添加角色")
    @PostMapping(value = "/create")
    public ObjectResult<Object> create(@Validated @RequestBody SysRole role) {
        boolean success = roleService.create(role);
        if (success) {
            return ObjectResult.success();
        }
        return ObjectResult.failed();
    }

    @ApiOperation("修改角色")
    @PostMapping(value = "/update/{id}")
    public ObjectResult<Object> update(
            @PathVariable Long id,
            @Validated @RequestBody SysRole role) {
        role.setId(id);
        boolean success = roleService.updateById(role);
        if (success) {
            return ObjectResult.success();
        }
        return ObjectResult.failed();
    }

    @ApiOperation("批量删除角色")
    @PostMapping(value = "/delete")
    public ObjectResult<Object> delete(@RequestParam("ids") List<Long> ids) {
        boolean success = roleService.delete(ids);
        if (success) {
            return ObjectResult.success();
        }
        return ObjectResult.failed();
    }


    @ApiOperation("获取所有角色")
    @GetMapping(value = "/listAll")
    public ObjectResult<List<SysRole>> listAll() {
        List<SysRole> roleList = roleService.list();
        return ObjectResult.success(roleList);
    }

    @ApiOperation("角色分页列表")
    @GetMapping(value = "/list")
    public ObjectResult<PageResult<SysRole>> list(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Page<SysRole> roleList = roleService.list(keyword, pageSize, pageNum);
        return ObjectResult.success(PageResult.restPage(roleList));
    }

    @ApiOperation("修改角色状态")
    @PostMapping(value = "/updateStatus/{id}")
    public ObjectResult<Object> updateStatus(@PathVariable Long id, @RequestParam(value = "status") Integer status) {
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setStatus(status);
        boolean success = roleService.updateById(sysRole);
        if (success) {
            return ObjectResult.success();
        }
        return ObjectResult.failed();
    }

    @ApiOperation("获取角色相关菜单")
    @GetMapping(value = "/listMenu/{roleId}")
    public ObjectResult<List<SysMenu>> listMenu(@PathVariable Long roleId) {
        List<SysMenu> roleList = roleService.listMenu(roleId);
        return ObjectResult.success(roleList);
    }

    @ApiOperation("获取角色相关资源")
    @GetMapping(value = "/listResource/{roleId}")
    public ObjectResult<List<SysResource>> listResource(@PathVariable Long roleId) {
        List<SysResource> roleList = roleService.listResource(roleId);
        return ObjectResult.success(roleList);
    }

    @ApiOperation("给角色分配菜单")
    @PostMapping(value = "/allocMenu")
    public ObjectResult<Object> allocMenu(@RequestParam Long roleId, @RequestParam List<Long> menuIds) {
        int count = roleService.allocMenu(roleId, menuIds);
        return ObjectResult.success(count);
    }

    @ApiOperation("给角色分配资源")
    @PostMapping(value = "/allocResource")
    public ObjectResult<Object> allocResource(@RequestParam Long roleId, @RequestParam List<Long> resourceIds) {
        int count = roleService.allocResource(roleId, resourceIds);
        return ObjectResult.success(count);
    }

}
