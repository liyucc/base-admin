package com.hongtech.tiny.modules.sys.controller;

import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.modules.sys.entity.SysResourceCategory;
import com.hongtech.tiny.modules.sys.service.SysResourceCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 后台资源分类管理Controller
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "后台资源分类")
@RequestMapping("/resourceCategory")
public class SysResCategoryController {

    private final SysResourceCategoryService resourceCategoryService;

    @ApiOperation("查询所有后台资源分类")
    @GetMapping(value = "/listAll")
    public ObjectResult<List<SysResourceCategory>> listAll() {
        List<SysResourceCategory> resourceList = resourceCategoryService.listAll();
        return ObjectResult.success(resourceList);
    }

    @ApiOperation("添加后台资源分类")
    @PostMapping(value = "/create")
    public ObjectResult<Object> create(@Validated @RequestBody SysResourceCategory sysResourceCategory) {
        boolean success = resourceCategoryService.create(sysResourceCategory);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("修改后台资源分类")
    @PostMapping(value = "/update/{id}")
    public ObjectResult<Object> update(
            @PathVariable Long id,
            @Validated @RequestBody SysResourceCategory sysResourceCategory) {
        sysResourceCategory.setId(id);
        boolean success = resourceCategoryService.updateById(sysResourceCategory);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

    @ApiOperation("根据ID删除后台资源")
    @PostMapping(value = "/delete/{id}")
    public ObjectResult<Object> delete(@PathVariable Long id) {
        boolean success = resourceCategoryService.removeById(id);
        if (success) {
            return ObjectResult.success();
        } else {
            return ObjectResult.failed();
        }
    }

}
