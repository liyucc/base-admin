package com.hongtech.tiny.modules.app.controller;

import com.hongtech.tiny.common.response.ObjectResult;
import com.hongtech.tiny.modules.sys.entity.RegionEntity;
import com.hongtech.tiny.modules.sys.service.RegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * APP测试接口
 */
@RestController
@RequestMapping("/app")
@Api(tags = "APP公共接口")
public class CommonController {

    @Autowired
    private RegionService regionService;

    @GetMapping("/getProvinceList")
    @ApiOperation("省份列表")
    public ObjectResult<List<RegionEntity>> getProvinceList() {
        List<RegionEntity> list = regionService.getProvinceList();
        return ObjectResult.success(list);
    }

    @GetMapping("/getCityList")
    @ApiOperation("城市列表")
    public ObjectResult<List<RegionEntity>> getCityList(String provinceId) {
        List<RegionEntity> list = regionService.getCityListByProvinceId(provinceId);
        return ObjectResult.success(list);
    }

}
