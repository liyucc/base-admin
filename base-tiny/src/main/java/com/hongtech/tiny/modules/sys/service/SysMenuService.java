package com.hongtech.tiny.modules.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.sys.dto.SysMenuNode;
import com.hongtech.tiny.modules.sys.entity.SysMenu;
import com.hongtech.tiny.modules.sys.vo.vo.RouterVo;

import java.util.List;

/**
 * 后台菜单管理Service
 */
public interface SysMenuService extends IService<SysMenu> {
    /**
     * 创建后台菜单
     */
    boolean create(SysMenu sysMenu);

    /**
     * 修改后台菜单
     */
    boolean update(Long id, SysMenu sysMenu);

    /**
     * 分页查询后台菜单
     */
    Page<SysMenu> list(Long parentId, Integer pageSize, Integer pageNum);

    /**
     * 树形结构返回所有菜单列表
     */
    List<SysMenuNode> treeList();

    /**
     * 修改菜单显示状态
     */
    boolean updateHidden(Long id, Integer hidden);

    /**
     * 根据用户Id查询用户菜单
     */
    List<SysMenuNode> listMenuTreeByUserId(Long id);

    /**
     * 构建前端路由
     */
    List<RouterVo> buildMenus(List<SysMenuNode> menus);

}
