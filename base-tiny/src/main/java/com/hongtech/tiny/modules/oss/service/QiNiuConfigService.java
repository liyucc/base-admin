package com.hongtech.tiny.modules.oss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hongtech.tiny.modules.oss.entity.QiniuConfigEntity;

public interface QiNiuConfigService extends IService<QiniuConfigEntity> {

    /**
     * 查询配置
     * @return QiniuConfig
     */
    QiniuConfigEntity getConfig();

    /**
     * 保存
     * @param type 类型
     */
    void saveConfig(QiniuConfigEntity type);

    /**
     * 更新
     * @param type 类型
     */
    void updateType(String type);

}
