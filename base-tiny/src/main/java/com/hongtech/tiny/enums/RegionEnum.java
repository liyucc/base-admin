package com.hongtech.tiny.enums;

import com.hongtech.tiny.common.exception.CustomException;
import com.hongtech.tiny.common.response.ResultCode;

public enum RegionEnum {

    LEVEL_PROVINCE(1, "省份"),

    LEVEL_CITY(2, "城市"),

    LEVEL_DISTRICTS(3, "地区"),

    ;

    private Integer code;

    private String text;

    RegionEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    private static final RegionEnum[] enums = RegionEnum.values();

    public static RegionEnum getRegionEnumByCode(String code) {
        for (RegionEnum enm : enums) {
            if (enm.getCode().equals(code)) {
                return enm;
            }
        }
        throw new CustomException(ResultCode.VALIDATE_FAILED);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
