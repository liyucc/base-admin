package com.hongtech.tiny.enums;

import com.hongtech.tiny.common.exception.CustomException;
import com.hongtech.tiny.common.response.ResultCode;

public enum CommonEnum {

    HOME_PAGE(1, "首页"),

    DETAIL_PAGE(2, "详情页"),

    ;

    private Integer code;

    private String text;

    CommonEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    private static final CommonEnum[] enums = CommonEnum.values();

    public static CommonEnum getEsActionEnumByCode(String code) {
        for (CommonEnum enm : enums) {
            if (enm.getCode().equals(code)) {
                return enm;
            }
        }
        throw new CustomException(ResultCode.VALIDATE_FAILED);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
