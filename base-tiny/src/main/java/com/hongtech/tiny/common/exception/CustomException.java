package com.hongtech.tiny.common.exception;

import com.hongtech.tiny.common.response.IErrorCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 自定义API异常
 */
@Setter
@Getter
public class CustomException extends RuntimeException {

    private IErrorCode errorCode;

    public CustomException(IErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public CustomException(String message) {
        super(message);
    }

    public CustomException(Throwable cause) {
        super(cause);
    }

    public CustomException(String message, Throwable cause) {
        super(message, cause);
    }

}
