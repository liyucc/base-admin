package com.hongtech.tiny.common.exception;

import com.hongtech.tiny.common.response.IErrorCode;

/**
 * 断言处理类，用于抛出各种API异常
 */
public class Asserts {

    private Asserts() {}

    public static void fail(String message) {
        throw new CustomException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new CustomException(errorCode);
    }

}
