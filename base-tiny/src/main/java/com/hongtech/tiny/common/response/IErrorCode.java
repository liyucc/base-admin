package com.hongtech.tiny.common.response;

/**
 * 封装API的错误码
 */
public interface IErrorCode {

    long getCode();

    String getMessage();

}
