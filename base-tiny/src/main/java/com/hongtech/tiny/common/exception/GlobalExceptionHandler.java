package com.hongtech.tiny.common.exception;

import com.hongtech.tiny.common.response.ObjectResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ObjectResult<?> handle(Exception e) {
        if(e instanceof CustomException) {
            CustomException exception = (CustomException)e;
            if (exception.getErrorCode() != null) {
                return ObjectResult.failed(exception.getErrorCode());
            }
        }
        return ObjectResult.failed();
    }

}
