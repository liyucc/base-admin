package com.hongtech.tiny;

import com.hongtech.tiny.common.service.RedisService;
import com.hongtech.tiny.modules.knowledge.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class RedisServiceTest {

    @Resource
    private RedisService redisService;

    @Test
    public void testRedisSet() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setUsername("hongTech");
        userEntity.setMobile("1234567890");
        redisService.set("redisTest:key1", userEntity, 3*60L);
        System.out.println("设置成功");
    }

    @Test
    public void testRedisGet() {
        UserEntity result = (UserEntity) redisService.get("redisTest:key1");
        System.out.println("获取成功：" + result.getMobile());
    }

    @Test
    public void testRedisDel() {
        Boolean result = redisService.del("redisTest:key1");
        System.out.println("删除成功：" + result);
    }

}
