package com.hongtech.tiny;


import com.hongtech.tiny.common.service.RedisService;
import com.hongtech.tiny.common.service.RedisWatcherLockService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


@SpringBootTest
public class DistributeLockServiceTest {

    private static final Logger _log = LoggerFactory.getLogger(DistributeLockServiceTest.class);

    @Resource
    private RedisWatcherLockService distributeLockService;

    @Test
    public void test1_lock() {
        boolean result = distributeLockService.lockByJedis("key1", "hongTech", 0,3*60);
        System.out.println("获取锁：" + result);
    }

    @Test
    public void test2_unLock() {
        boolean result = distributeLockService.unLockByJedis("key1", "hongTech");
        System.out.println("释放锁：" + result);
    }

    @Test
    public void test3_lock() {
        boolean result = distributeLockService.lockByRedisTemplate("key1", "hongTech", 3*60);
        System.out.println("获取锁：" + result);
    }

    @Test
    public void test4_unLock() {
        boolean result = distributeLockService.unLockByRedisTemplate("key1", "hongTech");
        System.out.println("释放锁：" + result);
    }

}
