package com.hongtech.tiny;

import com.hongtech.tiny.common.service.RedisWatcherLockService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

@SpringBootTest
public class MultiThreadTest {

    private static final Logger _log = LoggerFactory.getLogger(MultiThreadTest.class);

    @Resource
    private RedisWatcherLockService distributeLockService;

    @Resource
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 多线程模拟锁抢占
     */
    @Test
    public void test1() throws InterruptedException {
        for (int i = 1; i < 11; i++) {
            threadPoolExecutor.execute(new Task());
        }
        Thread.sleep(120000);
        threadPoolExecutor.shutdown();
    }

    class Task implements Runnable {
        @Override
        public void run() {
            String key = "key2";
            String value = UUID.randomUUID().toString();
            // 抢占锁
            distributeLockService.lockByRedisTemplate(key, value,3*60);
            // 模拟业务处理
            try {
                Thread.sleep(new Random().nextInt(6) * 100);
            } catch (InterruptedException e) {
                _log.info("Exception stack trace: ", e);
            }
            // 释放锁
            distributeLockService.unLockByRedisTemplate(key, value);
        }
    }

}
