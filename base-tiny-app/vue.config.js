const webpack = require('webpack')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const UglifyPlugin = require('uglifyjs-webpack-plugin')
const productionGzipExtensions = ['js', 'css'] // 对文件进行压缩
const Timestamp = new Date().getTime(); // 防止http的缓存每次打包用时间戳进行区分

module.exports = {
  publicPath: './',
  css: {
    // 是否使用css分离插件
    extract: process.env.NODE_ENV === 'production',
    // 开启 CSS source maps
    sourceMap: false,
    // css预设器配置项
    loaderOptions: {},
  },
  productionSourceMap: false,
  configureWebpack: {
    output: { // 输出重构  打包编译后的 文件名称
      filename: `js/[name]${Timestamp}.js`,
      chunkFilename: `js/[name]${Timestamp}.js`
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), //该插件能够使得指定目录被忽略，从而使得打包变快，文件变小
      // 下面是下载的插件的配置
      new CompressionWebpackPlugin({
        algorithm: 'gzip', // 压缩算法
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'), // 处理所有匹配此 {RegExp} 的资源
        threshold: 10240, // 只处理比这个值大的资源。按字节计算
        minRatio: 0.8 // 只有压缩率比这个值小的资源才会被处理
      }),
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 5, // 控制打包生成js的个数
        minChunkSize: 100
      }),
    ],
    optimization: {
      minimizer: [new UglifyPlugin({
        uglifyOptions: {
          warnings: false,
          compress: {
            drop_console: true, // console
            drop_debugger: true, // debugger
            pure_funcs: ['console.log'] // 移除console
          }
        }
      })]
    },
    externals: { // 在这里配置后,减少了压缩的包内容,需要在public/index.html通过cdn方式再引入,注意对应的版本
      vue: "Vue",
      "vue-router": "VueRouter"
    }
  },
  devServer: {
    host: '0.0.0.0',
    port: 8087,
    open: false,
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: `http://localhost:8088`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    },
    disableHostCheck: true
  }
}
