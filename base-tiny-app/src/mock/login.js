const userInfo = {
    user: {
      userId: 1,
      username: "123456",
      mobile: "2554635665",
      headPic: "https://news.mydrivers.com/Img/20110518/04481549.png",
      gender: 0,
      introduction: "用户简介",
      birthday: "1994-11-19",
      authStatus: 0
    },
    token: "sasdsdwewqfdsgrfggrdasdawdw",
    expire: 0,
}

export const login = req => {
  req = JSON.parse(req.body);
  return { code: 200, userInfo };
};

export const getUserInfo = req => {
  return { code: 200, userInfo };
}

export const logout = req => {
  return req ? null : "";
};

export const editPassword = req => {
  return { code: 200, message: "ok" };
};
