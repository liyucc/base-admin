import Mock from "mockjs";
import * as Login from "./login";

// 登录相关
Mock.mock("/login", "post", Login.login);
Mock.mock("/logout", "post", Login.logout);
Mock.mock(/\/getUserInfo*?/, "get" ,Login.getUserInfo);

export default Mock;
