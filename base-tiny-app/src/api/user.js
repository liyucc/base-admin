import http from '@/utils/http'

// 获取省份
export const getProvinceList = () => {
	return http.get(`/app/getProvinceList`)
}

// 获取城市
export const getCityList = (provinceId) => {
	return http.get(`/app/getCityList?provinceId=${provinceId}`)
}

// 保存个人资料
export const saveUserInfo = (params) => {
	return http.post(`/app/saveUserInfo`, params)
}

// 保存用户头像
export const saveUserAvatar = (params) => {
	return http.post(`/app/saveUserAvatar`, params)
}

// 获取个人资料
export const getUserInfo = (params) => {
	return http.get(`/app/getUserInfo`, params)
}

// 保存我的认证
export const saveUserAuth = (params) => {
	return http.post(`/app/saveUserAuth`, params)
}

// 获取认证信息
export const getUserAuth = () => {
	return http.get(`/app/getUserAuth`)
}