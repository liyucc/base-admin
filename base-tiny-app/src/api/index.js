import http from '@/utils/http'

export const baseUrl = process.env.VUE_APP_BASE_API;

export const fileUrl = process.env.VUE_APP_FILE_URL;

// 获取列表横幅数据
export const getBannerList = (params) => {
	return http.get('/app/banner/list', params);
}

// 获取首页常见病
export const getHomeList = (params) => {
	return http.get('/app/firstAid/page', params);
}

// 登录
export const login = (params) => {
	return http.post('/app/login', params);
};

// 获取短信验证码
export const getCode = (params) => {
	return http.get(`/app/getCode`, params);
}

// 验证码校验
export const checkCode = (params) => {
	return http.get('/app/checkCode', params);
}

// 注册新用户
export const register = (params) => {
	return http.post('/app/register', params);
}

// 重置密码
export const resetPassword = (params) => {
	return http.get('/app/resetpasswd', params);
}

// 密码变更
export const editPassword = (params) => {
	return http.post('/app/editPassword', params);
}
