import http from '@/utils/http'

// 获取急救分类
export const listFirstAidType = () => {
	return http.get(`/app/firstAidType/list`)
}

// 获取急救知识
export const listFirstAidPage = (params) => {
	return http.get(`/app/firstAid/page`, params)
}

// 获取急救知识详情
export const getFirstAidDetail = (id) => {
	return http.get(`/app/firstAid/detail/${id}`)
}