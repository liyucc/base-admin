// 邮箱校验
export function isEmail(email) {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(email);
}

// 手机号校验
export function isMobile(str) {
  const reg = /^[1]([3-9])[0-9]{9}$/;
  return reg.test(str);
}

// 身份证校验
export function isCardNo(card) {
  // const reg = /^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
  const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  return reg.test(card);
}

// 正整数校验
export function isInt(num) {
  const reg = /^\+?[1-9][0-9]*$/;
  return reg.test(num);
}

// 数字英文字母校验
export function isNumberOrLetter(str) {
  const reg = /^\w+$/;
  return reg.test(str);
}