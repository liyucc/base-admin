export function autoSize () {
  let doc = document, win = window;
  let docEl = doc.documentElement,
  resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
  recalc = function () {
    let clientWidth = docEl.clientWidth;
    if (!clientWidth) return;
    // 这里的750 取决于设计稿的宽度
    if (clientWidth >= 750) {
      docEl.style.fontSize = '100px';
    } else {
      docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
    }
  };
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
  doc.addEventListener('DOMContentLoaded', recalc, false);
}

// 字符串截取
export function trunc(value, length, separator) {
  let len = parseInt(length);
  let codes = separator ? separator.toString() : '...';
  return value.length < length ? value : value.substring(0, len) + codes;
}

// 手机或身份证号隐藏中间位数start前面保留的字符个数,end后面保留的字符个数
export function hideNum(str, start, end) {
  if (!str) return '';

  const length = str.length;
  const code = '*';

  let reStr =
    str.substr(0, start) +
    code.repeat(length - start - end) +
    str.substring(length - end);

  return reStr;
}
