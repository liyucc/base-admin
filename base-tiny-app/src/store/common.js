export default {
  state:{
    banners:[]
  },
  getters:{
    banners: state => state.banners
  },
  mutations:{
    setBanners(state, banners){
      state.banners = banners;
    }
  },
  actions:{

  }
}