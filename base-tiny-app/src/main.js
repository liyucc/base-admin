import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import moment from "moment";
import "moment/locale/zh-cn";
import '@/assets/app.scss'
import { autoSize } from "./utils";
import { Lazyload } from 'vant';

autoSize();

Vue.use(Lazyload);

Vue.use(Lazyload, {
  lazyComponent: true,
});

Vue.use(Vant)

Vue.prototype.$moment = moment;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
