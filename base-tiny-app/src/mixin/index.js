export default {
  methods: {
    //容器绑定滚动到底部触发事件
    bindScroll(container,fn){
      let box = this.$refs[container];
      box.addEventListener(
        'scroll',
        e => {
          this.handleScroll(e, fn);
        },
        true 
      );
    },
    handleScroll(e, fn) {
      let scrollTop = e.target.scrollTop;
      let windowHeight = e.target.clientHeight;
      let scrollHeight = e.target.scrollHeight;
      if (scrollHeight - (scrollTop + windowHeight) < 1 ) {
        if(fn && typeof fn === 'function'){
          fn();
        }
      }
    }
  }
};
